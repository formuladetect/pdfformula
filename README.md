# Train a new model starting from pre-trained COCO weights
- python formulaFinder.py train --dataset=/path/to/dataset/ --model=coco


# Run inference 
- python formulaFinder.py splash --weights=/logs/weights --image=/path/to/image

# Installation

- Clone this repository

# Install dependencies



- pip3 install -r requirements.txt
- Run setup from the repository root directory


- python3 setup.py install

- Download pre-trained COCO weights (mask_rcnn_coco.h5) from the releases page.
- https://github.com/matterport/Mask_RCNN/releases
